const mainTabItems = document.querySelectorAll('.main-tab-item');
const mainTabContentItems = document.querySelectorAll('.main-tab-content-item')

function selectItem(e) {

    removeBorder();
    removeShow();
    this.classList.add('tab-border');
 
    // console.log(this.id)
    const mainTabContentItem = document.querySelector(`#${this.id}-content`);
    mainTabContentItem.classList.add('show')
 
}

function removeBorder() {
    mainTabItems.forEach(item => item.classList.remove('tab-border'))
}

function removeShow() {
    mainTabContentItems.forEach(item => item.classList.remove('show'))
}

mainTabItems.forEach(item => item.addEventListener('click',selectItem));