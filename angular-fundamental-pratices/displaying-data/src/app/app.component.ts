import { Component } from '@angular/core';
import { Hero } from './hero';


@Component({
  selector: 'app-root',
  template: `
  <h1>{{title}}</h1>
  <input (keyup.enter)="onKey($event)">
  <p>{{value}}</p>
  <h2>loop-back</h2>
  <input #box (keyup)="0">
  <p>{{box.value}}</p>
  
  <h2 [appHighlight]="">Day la highlight w/ directive</h2>

  <h1>My First Attribute Directive</h1>

<h4>Pick a highlight color</h4>
<div>
  <input type="radio" name="colors" (click)="color='lightgreen'">Green
  <input type="radio" name="colors" (click)="color='yellow'">Yellow
  <input type="radio" name="colors" (click)="color='cyan'">Cyan
</div>
<p [appHighlight]="color">Highlight me!</p>

<p [appHighlight]="color" defaultColor="violet">
  Highlight me too!
</p>

<p *appUnless="condition" class="unless a">
  (A) This paragraph is displayed because the condition is false.
</p>

<button (click)="changeState()">Change state</button>
<p *appUnless="!condition" class="unless b">
  (B) Although the condition is true,
  this paragraph is displayed because appUnless is set to false.
</p>


<p>The hero's birthday is {{ birthday | date:format }}</p>
  <button (click)="toggleFormat()">Toggle Format</button>


  <h2>Power Boost Calculator</h2>
    <div>Normal power: <input [(ngModel)]="power"></div>
    <div>Boost factor: <input [(ngModel)]="factor"></div>
    <p>
      Super Hero Power: {{power | expo: factor}}
    </p>
`,
  styleUrls: ['./app.component.css']
})
export class AppComponent {


  power = 5
  factor = 1

  title = 'Test'
  value : string
  color :string
  condition = true
  toggle = true
  birthday = new Date(1988, 3, 15);

  get format() {
    return this.toggle ? 'shortDate' : 'fullDate';
  }

  toggleFormat () {
    this.toggle = !this.toggle
  }

  changeState() {
    if (this.condition === true) {this.condition = false}
    else (this.condition = true)
  }

  onKey(e : any) {
    this.value += e.target.value + ' | ';
  }
  

  // constructor() {}
}
